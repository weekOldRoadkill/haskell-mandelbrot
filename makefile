build: haskell-mandelbrot


run: build
	mangohud --dlsym ./haskell-mandelbrot


haskell-mandelbrot: app/* haskell-mandelbrot.cabal makefile stack.yaml
	stack build
	cp -fr .stack-work/install/*/*/*/bin/haskell-mandelbrot .
	strip haskell-mandelbrot
	upx -9 haskell-mandelbrot


clean:
	stack purge
	rm -fr stack.yaml.lock haskell-mandelbrot
