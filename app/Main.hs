{- Language Extensions -}
{-# Language OverloadedStrings #-}
{-# Language TemplateHaskell #-}




{- Imports -}
import qualified Graphics.Rendering.OpenGL as GL
import qualified Data.ByteString.Char8 (pack)
import Data.FileEmbed
import qualified SDL
import Foreign




{- Compile Shader Function -}
compileShader :: GL.ShaderType -> String -> IO GL.Shader
compileShader type' src = do

	shader <- GL.createShader type'
	GL.shaderSourceBS shader GL.$= Data.ByteString.Char8.pack src
	GL.compileShader shader

	log' <- GL.get $ GL.shaderInfoLog shader
	if log' == "" then return () else putStrLn log'

	status <- GL.get $ GL.compileStatus shader
	if status then return shader else error "Shader compilation failed."




{- Link Program Function -}
linkProgram :: [GL.Shader] -> IO GL.Program
linkProgram shaders = do

	program <- GL.createProgram
	mapM_ (GL.attachShader program) shaders 
	GL.linkProgram program

	log' <- GL.get $ GL.programInfoLog program
	if log' == "" then return () else putStrLn log'

	status <- GL.get $ GL.linkStatus program
	if status then return program else error "Program linking failed."




{- Main Function -}
main :: IO ()
main = do


	-- SDL Initialization
	SDL.initializeAll
	window <- SDL.createWindow "Game" SDL.defaultWindow {
		SDL.windowGraphicsContext = SDL.OpenGLContext SDL.defaultOpenGL {
			SDL.glProfile = SDL.Core SDL.Normal 4 3,
			SDL.glMultisampleSamples = 4},
		SDL.windowInitialSize = SDL.V2 800 800}


	-- OpenGL Initialization
	context <- SDL.glCreateContext window
	SDL.swapInterval SDL.$= SDL.ImmediateUpdates

	vao <- GL.genObjectName
	GL.bindVertexArrayObject GL.$= Just vao

	vertShader <- compileShader GL.VertexShader $(embedStringFile "app/vert.glsl")
	fragShader <- compileShader GL.FragmentShader $(embedStringFile "app/frag.glsl")
	program <- linkProgram [vertShader, fragShader]
	GL.deleteObjectNames [vertShader, fragShader]
	GL.currentProgram GL.$= Just program

	let verts = [
		GL.Vertex2 (-1) (-1),
		GL.Vertex2 (-1) 1,
		GL.Vertex2 1 (-1),
		GL.Vertex2 1 1] :: [GL.Vertex2 Float]
	let size list = fromIntegral (length list*(sizeOf $ head list))

	vbo <- GL.genObjectName
	GL.bindBuffer GL.ArrayBuffer GL.$= Just vbo
	withArray verts (\ptr -> GL.bufferData GL.ArrayBuffer GL.$= (size verts, ptr, GL.StaticDraw))

	let vertPos = GL.AttribLocation 0
	GL.vertexAttribArray vertPos GL.$= GL.Enabled
	GL.vertexAttribPointer vertPos GL.$= (GL.ToFloat, GL.VertexArrayDescriptor 2 GL.Float 0 nullPtr)


	-- Game Loop
	let loop x' y' scroll' = do


		-- Events
		SDL.pumpEvents
		events' <- SDL.pollEvents
		let events = map SDL.eventPayload events'


		-- Mouse Wheel Events
		let mouseWheelEvents = [e | e' <- events, let SDL.MouseWheelEvent e = e', case e' of
			SDL.MouseWheelEvent _ -> True
			_ -> False]

		let SDL.V2 _ scroll'' = sum [scroll''' | event <- mouseWheelEvents,
			let SDL.MouseWheelEventData _ _ scroll''' _ = event]
		let scroll = max (-100) $ min 10 (scroll'+scroll'')

		let scale = (9/8)**fromIntegral scroll


		-- Mouse Motion Events
		let mouseMotionEvents = [e | e' <- events, let SDL.MouseMotionEvent e = e', case e' of
			SDL.MouseMotionEvent _ -> True
			_ -> False]

		let SDL.V2 dx' dy' = sum [movement | event <- mouseMotionEvents,
			let SDL.MouseMotionEventData _ _ _ _ movement = event]
		let (dx, dy) = (scale*fromIntegral dx'/400, -scale*fromIntegral dy'/400) :: (Float, Float)

		let pressed = or [elem SDL.ButtonLeft pressed' | event <- mouseMotionEvents,
			let SDL.MouseMotionEventData _ _ pressed' _ _ = event]

		let (x, y) = if pressed then (max (-2) $ min 2 (x'+dx), max (-2) $ min 2 (y'+dy)) else (x', y')


		-- Rendering
		GL.uniform (GL.UniformLocation 0) GL.$= GL.Vertex3 x y scale
		GL.drawArrays GL.TriangleStrip 0 4
		SDL.glSwapWindow window


		-- Looping
		if elem SDL.QuitEvent events then return () else loop x y scroll

	loop 0.5 0 0


	-- OpenGL De-initialization
	GL.vertexAttribArray vertPos GL.$= GL.Disabled
	GL.deleteObjectName vbo
	GL.deleteObjectName program
	GL.deleteObjectName vao


	-- SDL De-initialization
	SDL.glDeleteContext context
	SDL.destroyWindow window
	SDL.quit
