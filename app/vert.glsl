// Pre-processing
#version 430 core




// In
layout (location = 0) in vec2 pos;




// Out
out vec2 coords;




// Main Function
void main() {
	gl_Position.xy = pos;
	coords = pos;
}
