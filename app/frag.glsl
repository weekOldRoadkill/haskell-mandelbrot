// Pre-processing
#version 430 core




// In
in vec2 coords;

layout (location = 0) uniform vec3 pos;




// Out
out vec4 color;




// Color from Hue Function
float fromHue_(float x) {
	return max(0, min(1, abs(6*mod(x, 1)-3)-1));
}

vec3 fromHue(float hue) {
	return vec3(fromHue_(hue), fromHue_(hue-.33333333), fromHue_(hue+.33333333));
}




// Mandelbrot Function
vec3 mandelbrot(vec2 c) {
	vec2 z = vec2(0);

	for(uint i = 0; i < 512; i++) {
		z = vec2(dot(z, vec2(z.x, -z.y)), 2*z.x*z.y)+c;
		if(dot(z, z) > 4) return fromHue(pow(1-1/float(i), 16));
	}

	return vec3(0);
}




// Main Function
void main() {
	color = vec4(mandelbrot(pos.z*coords.xy-pos.xy), 1);
}
